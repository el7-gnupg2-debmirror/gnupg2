# README for compiling gnupg2 on el7
## Goals
Get debmirror to work on CentOS 7 as of 2020-03-12.

## Steps
1. Compile libgpg-error
2. Customize and compile libgcrypt
3. Compile npath
4. Compile libassuan
5. Compile libksba
6. Compile gnupg2

## Upstream locations
Download any source tarballs from https://gnupg.org/download/index.html
Get RH-hobbled contents from http://albion320.no-ip.biz/mirror/fedora/linux/updates/30/Everything/SRPMS/Packages/l/
https://src.fedoraproject.org/rpms/npth and other package names.

## Changes to upstream
All packages required at a minimum undoing a fedora-ism
Replace `%ldconfig_scriplets` with
```
%{?ldconfig:
%ldconfig_post %{?*} %{-n:-n %{-n*}}
%ldconfig_postun %{?*} %{-n:-n %{-n*}}
}
```
libgcrypt needs a custom name
